import { RouterModule, Routes } from '@angular/router';
import {LoginComponent} from './views/login/login.component';
import { EmployeeListPageComponent } from './views/employee-list-page/employee-list-page.component';
import { EmployeeListComponent } from './views/employee-list/employee-list.component';
import { EmployeeFormAddComponent } from './views/employee-form-add/employee-form-add.component';
import { EmployeeComponent } from './views/employee/employee.component';
import { EmployeeDetailComponent } from './views/employee-detail/employee-detail.component';
import { NavBarComponent } from './views/nav-bar/nav-bar.component';

const routes: Routes = [
  { path : '', component : LoginComponent},
  { path: 'login', component: LoginComponent },
  { path: 'employee-list-page', component: EmployeeListPageComponent },
  { path: 'employee-list', component: EmployeeListComponent },
  { path: 'employee-form-add', component: EmployeeFormAddComponent },
  { path: 'employee', component: EmployeeComponent },
  { path: 'employee-detail', component: EmployeeDetailComponent },
  { path: 'nav-bar', component: NavBarComponent },
];

export const routing = RouterModule.forRoot(routes);
