import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {routing} from './app.routing';
import {ReactiveFormsModule, FormsModule} from '@angular/forms';
import {DataTableModule} from 'angular-6-datatable';
import { DataTablesModule } from 'angular-datatables';
import { HttpClientModule } from '@angular/common/http';
import { Ng2SearchPipeModule } from 'ng2-search-filter'; // importing the module
import { Ng2OrderModule } from 'ng2-order-pipe'; // importing the module
import { NgSelectModule } from '@ng-select/ng-select';
import { MatSliderModule } from '@angular/material/slider';
import { MaterialModule } from './material/material.module';
import { NgxCurrencyModule } from 'ngx-currency';
import { NotifierModule, NotifierOptions } from 'angular-notifier';
// Import your library
import { AlertsModule } from 'angular-alert-module';

import { AppComponent } from './app.component';
import { LoginComponent } from './views/login/login.component';
import { EmployeeListPageComponent } from './views/employee-list-page/employee-list-page.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { EmployeeListComponent } from './views/employee-list/employee-list.component';
import { EmployeeFormAddComponent } from './views/employee-form-add/employee-form-add.component';
import { EmployeeComponent } from './views/employee/employee.component';
import { EmployeeDetailComponent } from './views/employee-detail/employee-detail.component';
import { NavBarComponent } from './views/nav-bar/nav-bar.component';
export const customCurrencyMaskConfig = {
  align: 'left',
  allowNegative: false,
  allowZero: true,
  decimal: ',',
  precision: 0,
  prefix: '',
  suffix: '',
  thousands: '.',
  nullable: false
};
const customNotifierOptions: NotifierOptions = {
  position: {
    horizontal: {
      position: 'left',
      distance: 12
    },
    vertical: {
      position: 'bottom',
      distance: 12,
      gap: 10
    }
  },
  theme: 'material',
  behaviour: {
    autoHide: 5000,
    onClick: 'hide',
    onMouseover: 'pauseAutoHide',
    showDismissButton: true,
    stacking: 4
  },
  animations: {
    enabled: true,
    show: {
      preset: 'slide',
      speed: 300,
      easing: 'ease'
    },
    hide: {
      preset: 'fade',
      speed: 300,
      easing: 'ease',
      offset: 50
    },
    shift: {
      speed: 300,
      easing: 'ease'
    },
    overlap: 150
  }
};
@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    EmployeeListPageComponent,
    EmployeeListComponent,
    EmployeeFormAddComponent,
    EmployeeComponent,
    EmployeeDetailComponent,
    NavBarComponent
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    FormsModule,
    routing,
    DataTableModule,
    DataTablesModule,
    HttpClientModule,
    Ng2SearchPipeModule, // including into imports
    Ng2OrderModule, // add here
    NgSelectModule,
    BrowserAnimationsModule,
    MatSliderModule,
    MaterialModule,
    NgxCurrencyModule.forRoot(customCurrencyMaskConfig),
    AlertsModule.forRoot(),
    NotifierModule.withConfig(customNotifierOptions)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
