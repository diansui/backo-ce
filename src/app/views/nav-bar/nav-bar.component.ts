import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.css']
})
export class NavBarComponent implements OnInit {

  userName;
  constructor(private router: Router) { }

  ngOnInit() {
    this.userName = JSON.parse(localStorage.getItem('user'));
  }

  logout() {
    localStorage.setItem('user', JSON.stringify(''));
    this.router.navigate(['']);
  }

}
