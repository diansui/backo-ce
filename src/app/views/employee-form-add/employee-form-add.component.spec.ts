import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmployeeFormAddComponent } from './employee-form-add.component';

describe('EmployeeFormAddComponent', () => {
  let component: EmployeeFormAddComponent;
  let fixture: ComponentFixture<EmployeeFormAddComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmployeeFormAddComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmployeeFormAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
