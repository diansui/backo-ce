import { ElementRef, AfterViewInit, Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { Subject } from 'rxjs';
import {Router} from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {formatDate} from '@angular/common';
// import {moment} from 'ngx-bootstrap/chronos/test/chain';
// import moment from 'moment';

@Component({
  selector: 'app-employee-list-page',
  templateUrl: './employee-list-page.component.html',
  styleUrls: ['./employee-list-page.component.css']
})
export class EmployeeListPageComponent implements OnInit {
  public formGroup: FormGroup;
  public data: any;
  public groupData: any;
  public selectGroup: any;
  selectedId;
  title = 'Data Employee';
  searchText;
  selectedGroup;
  // Id untuk View
  @ViewChild('Viewusername') Viewusername: ElementRef;
  @ViewChild('ViewfirstName') ViewfirstName: ElementRef;
  @ViewChild('ViewlastName') ViewlastName: ElementRef;
  @ViewChild('Viewemail') Viewemail: ElementRef;
  @ViewChild('ViewbirthDate') ViewbirthDate: ElementRef;
  @ViewChild('ViewbasicSalary') ViewbasicSalary: ElementRef;
  @ViewChild('Viewstatus') Viewstatus: ElementRef;
  @ViewChild('Viewgroup') Viewgroup: ElementRef;
  @ViewChild('Viewdescription') Viewdescription: ElementRef;
  // Id untuk Update
  @ViewChild('Updusername') Updusername: ElementRef;
  @ViewChild('UpdfirstName') UpdfirstName: ElementRef;
  @ViewChild('UpdlastName') UpdlastName: ElementRef;
  @ViewChild('Updemail') Updemail: ElementRef;
  @ViewChild('UpdbirthDate') UpdbirthDate: ElementRef;
  @ViewChild('UpdbasicSalary') UpdbasicSalary: ElementRef;
  @ViewChild('Updstatus') Updstatus: ElementRef;
  @ViewChild('Updgroup') Updgroup: ElementRef;
  @ViewChild('Upddescription') Upddescription: ElementRef;
  emailPattern = '^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$';
  maxDateItem;
  minDate;
  maxDate;
  nextDay: Date;
  constructor(private router: Router,
              private formBuilder: FormBuilder) {
    this.createForm();
  }
  private createForm() {
    this.formGroup = this.formBuilder.group({
      'username': ['', Validators.required ],
      'firstname': ['', Validators.required ],
      'lastname': ['', Validators.required ],
      'email': ['', Validators.required ],
      'birthdate': ['', Validators.required ],
      'basicsalary': ['', Validators.required ],
      'status': ['', Validators.required ],
      'group': ['', Validators.required ],
      'description': ['', Validators.required ],
    });
  }

  ngOnInit() {
    this.data = [
      {
        'username': 'Clinton',
        'firstName': 'Harrison',
        'lastName': 'Chandler',
        'email': 'placerat@dictum.edu',
        'birthDate': '31/12/1969',
        'basicSalary': '6.000.000',
        'status': 'Divorced',
        'group': 'Customer Relations',
        'description': '08/12/2018'
      },
      {
        'username': 'Paki',
        'firstName': 'Simon',
        'lastName': 'Tate',
        'email': 'ante.lectus.convallis@nonfeugiat.ca',
        'birthDate': '31/12/1969',
        'basicSalary': '6.000.000',
        'status': 'Divorced',
        'group': 'Accounting',
        'description': '25/04/2020'
      },
      {
        'username': 'Duncan',
        'firstName': 'Hayden',
        'lastName': 'Lyle',
        'email': 'magna.et@maurisblanditmattis.ca',
        'birthDate': '31/12/1969',
        'basicSalary': '8.000.000',
        'status': 'Divorced',
        'group': 'Customer Relations',
        'description': '09/12/2019'
      },
      {
        'username': 'Justin',
        'firstName': 'Levi',
        'lastName': 'Bert',
        'email': 'eu@hendreritconsectetuercursus.org',
        'birthDate': '31/12/1969',
        'basicSalary': '5.000.000',
        'status': 'Married',
        'group': 'Quality Assurance',
        'description': '11/03/2020'
      },
      {
        'username': 'Guy',
        'firstName': 'Flynn',
        'lastName': 'Akeem',
        'email': 'aliquam@erat.com',
        'birthDate': '31/12/1969',
        'basicSalary': '2.000.000',
        'status': 'Common-Law',
        'group': 'Customer Relations',
        'description': '11/02/2019'
      },
      {
        'username': 'Zachary',
        'firstName': 'Tanek',
        'lastName': 'Chandler',
        'email': 'magna.Nam.ligula@orciPhasellusdapibus.edu',
        'birthDate': '31/12/1969',
        'basicSalary': '8.000.000',
        'status': 'Married',
        'group': 'Advertising',
        'description': '15/07/2019'
      },
      {
        'username': 'Elmo',
        'firstName': 'Burke',
        'lastName': 'Timothy',
        'email': 'Proin.eget.odio@acmattisornare.edu',
        'birthDate': '31/12/1969',
        'basicSalary': '8.000.000',
        'status': 'Married',
        'group': 'Sales and Marketing',
        'description': '02/12/2019'
      },
      {
        'username': 'Drake',
        'firstName': 'Jerry',
        'lastName': 'Francis',
        'email': 'volutpat.Nulla.facilisis@dignissimlacusAliquam.org',
        'birthDate': '31/12/1969',
        'basicSalary': '3.000.000',
        'status': 'Married',
        'group': 'Human Resources',
        'description': '25/11/2018'
      },
      {
        'username': 'Wade',
        'firstName': 'Kirk',
        'lastName': 'Matthew',
        'email': 'nunc@porttitortellusnon.org',
        'birthDate': '31/12/1969',
        'basicSalary': '3.000.000',
        'status': 'Common-Law',
        'group': 'Legal Department',
        'description': '30/08/2019'
      },
      {
        'username': 'Scott',
        'firstName': 'Thor',
        'lastName': 'Nasim',
        'email': 'faucibus@malesuadaIntegerid.edu',
        'birthDate': '31/12/1969',
        'basicSalary': '3.000.000',
        'status': 'Common-Law',
        'group': 'Quality Assurance',
        'description': '30/01/2020'
      },
      {
        'username': 'Caesar',
        'firstName': 'Vladimir',
        'lastName': 'Darius',
        'email': 'dolor.sit@volutpatnunc.com',
        'birthDate': '31/12/1969',
        'basicSalary': '7.000.000',
        'status': 'Common-Law',
        'group': 'Customer Relations',
        'description': '23/01/2020'
      },
      {
        'username': 'Hyatt',
        'firstName': 'Charles',
        'lastName': 'Felix',
        'email': 'nulla.Integer.vulputate@magnisdisparturient.org',
        'birthDate': '31/12/1969',
        'basicSalary': '2.000.000',
        'status': 'Married',
        'group': 'Customer Service',
        'description': '13/09/2019'
      },
      {
        'username': 'Yuli',
        'firstName': 'Fuller',
        'lastName': 'Ezekiel',
        'email': 'ad@erat.org',
        'birthDate': '31/12/1969',
        'basicSalary': '6.000.000',
        'status': 'Common-Law',
        'group': 'Quality Assurance',
        'description': '22/12/2019'
      },
      {
        'username': 'Randall',
        'firstName': 'Yasir',
        'lastName': 'Hyatt',
        'email': 'vitae.odio@ametanteVivamus.com',
        'birthDate': '31/12/1969',
        'basicSalary': '3.000.000',
        'status': 'Common-Law',
        'group': 'Media Relations',
        'description': '04/02/2020'
      },
      {
        'username': 'Cadman',
        'firstName': 'Fuller',
        'lastName': 'Hasad',
        'email': 'amet.ante@enimCurabitur.com',
        'birthDate': '31/12/1969',
        'basicSalary': '2.000.000',
        'status': 'Common-Law',
        'group': 'Legal Department',
        'description': '09/10/2019'
      },
      {
        'username': 'Levi',
        'firstName': 'Howard',
        'lastName': 'Raphael',
        'email': 'vitae.purus@nibhvulputate.co.uk',
        'birthDate': '31/12/1969',
        'basicSalary': '8.000.000',
        'status': 'Common-Law',
        'group': 'Legal Department',
        'description': '16/10/2018'
      },
      {
        'username': 'Erasmus',
        'firstName': 'Luke',
        'lastName': 'Quinlan',
        'email': 'tellus.sem@Etiamgravidamolestie.edu',
        'birthDate': '31/12/1969',
        'basicSalary': '8.000.000',
        'status': 'Common-Law',
        'group': 'Human Resources',
        'description': '25/10/2018'
      },
      {
        'username': 'Chaim',
        'firstName': 'Cruz',
        'lastName': 'Bradley',
        'email': 'odio.Phasellus.at@felis.net',
        'birthDate': '31/12/1969',
        'basicSalary': '5.000.000',
        'status': 'Single',
        'group': 'Legal Department',
        'description': '10/07/2020'
      },
      {
        'username': 'Raphael',
        'firstName': 'Forrest',
        'lastName': 'Grady',
        'email': 'dictum.magna.Ut@luctusfelis.ca',
        'birthDate': '31/12/1969',
        'basicSalary': '4.000.000',
        'status': 'Married',
        'group': 'Human Resources',
        'description': '15/08/2019'
      },
      {
        'username': 'Avram',
        'firstName': 'John',
        'lastName': 'Keegan',
        'email': 'accumsan.neque.et@dictum.com',
        'birthDate': '31/12/1969',
        'basicSalary': '3.000.000',
        'status': 'Common-Law',
        'group': 'Human Resources',
        'description': '22/02/2020'
      },
      {
        'username': 'Francis',
        'firstName': 'Armando',
        'lastName': 'Peter',
        'email': 'Nunc@idmagnaet.ca',
        'birthDate': '31/12/1969',
        'basicSalary': '3.000.000',
        'status': 'Single',
        'group': 'Human Resources',
        'description': '12/06/2019'
      },
      {
        'username': 'Chase',
        'firstName': 'Lyle',
        'lastName': 'Jerome',
        'email': 'velit.Cras@dui.co.uk',
        'birthDate': '31/12/1969',
        'basicSalary': '2.000.000',
        'status': 'Divorced',
        'group': 'Payroll',
        'description': '25/05/2020'
      },
      {
        'username': 'Lance',
        'firstName': 'Lucian',
        'lastName': 'Colt',
        'email': 'neque.pellentesque@NullainterdumCurabitur.org',
        'birthDate': '31/12/1969',
        'basicSalary': '6.000.000',
        'status': 'Married',
        'group': 'Legal Department',
        'description': '03/02/2019'
      },
      {
        'username': 'Kieran',
        'firstName': 'Cruz',
        'lastName': 'Carter',
        'email': 'tellus.lorem@ametluctusvulputate.com',
        'birthDate': '31/12/1969',
        'basicSalary': '7.000.000',
        'status': 'Single',
        'group': 'Asset Management',
        'description': '16/01/2019'
      },
      {
        'username': 'Chaim',
        'firstName': 'Nehru',
        'lastName': 'Steven',
        'email': 'blandit.enim.consequat@Donecegestas.net',
        'birthDate': '31/12/1969',
        'basicSalary': '4.000.000',
        'status': 'Single',
        'group': 'Tech Support',
        'description': '26/11/2019'
      },
      {
        'username': 'Reuben',
        'firstName': 'Kasper',
        'lastName': 'Gabriel',
        'email': 'morbi.tristique.senectus@magna.edu',
        'birthDate': '31/12/1969',
        'basicSalary': '7.000.000',
        'status': 'Common-Law',
        'group': 'Tech Support',
        'description': '17/11/2018'
      },
      {
        'username': 'Arden',
        'firstName': 'Honorato',
        'lastName': 'Noble',
        'email': 'scelerisque.neque.sed@semsempererat.com',
        'birthDate': '31/12/1969',
        'basicSalary': '8.000.000',
        'status': 'Common-Law',
        'group': 'Research and Development',
        'description': '19/06/2019'
      },
      {
        'username': 'Duncan',
        'firstName': 'Raymond',
        'lastName': 'Kasper',
        'email': 'nisl.arcu@luctus.edu',
        'birthDate': '31/12/1969',
        'basicSalary': '8.000.000',
        'status': 'Common-Law',
        'group': 'Legal Department',
        'description': '06/05/2020'
      },
      {
        'username': 'Quentin',
        'firstName': 'Boris',
        'lastName': 'Kenyon',
        'email': 'non.nisi@eleifend.edu',
        'birthDate': '31/12/1969',
        'basicSalary': '9.000.000',
        'status': 'Common-Law',
        'group': 'Sales and Marketing',
        'description': '24/02/2019'
      },
      {
        'username': 'Carl',
        'firstName': 'Gray',
        'lastName': 'Christopher',
        'email': 'quam.vel@Donec.ca',
        'birthDate': '31/12/1969',
        'basicSalary': '6.000.000',
        'status': 'Divorced',
        'group': 'Media Relations',
        'description': '01/09/2019'
      },
      {
        'username': 'Dean',
        'firstName': 'Keane',
        'lastName': 'Lee',
        'email': 'lorem@felis.com',
        'birthDate': '31/12/1969',
        'basicSalary': '3.000.000',
        'status': 'Single',
        'group': 'Tech Support',
        'description': '12/06/2020'
      },
      {
        'username': 'Ali',
        'firstName': 'Phillip',
        'lastName': 'Kibo',
        'email': 'amet@nec.edu',
        'birthDate': '31/12/1969',
        'basicSalary': '8.000.000',
        'status': 'Divorced',
        'group': 'Human Resources',
        'description': '11/10/2019'
      },
      {
        'username': 'Keaton',
        'firstName': 'Cadman',
        'lastName': 'Coby',
        'email': 'viverra@Aliquam.co.uk',
        'birthDate': '31/12/1969',
        'basicSalary': '8.000.000',
        'status': 'Common-Law',
        'group': 'Accounting',
        'description': '11/07/2019'
      },
      {
        'username': 'Jermaine',
        'firstName': 'Octavius',
        'lastName': 'Steel',
        'email': 'gravida.non.sollicitudin@mollis.co.uk',
        'birthDate': '31/12/1969',
        'basicSalary': '2.000.000',
        'status': 'Single',
        'group': 'Public Relations',
        'description': '16/08/2019'
      },
      {
        'username': 'Macaulay',
        'firstName': 'Merrill',
        'lastName': 'Buckminster',
        'email': 'ut.cursus.luctus@vitae.edu',
        'birthDate': '31/12/1969',
        'basicSalary': '3.000.000',
        'status': 'Married',
        'group': 'Payroll',
        'description': '17/05/2020'
      },
      {
        'username': 'Lucas',
        'firstName': 'Wade',
        'lastName': 'Chaim',
        'email': 'quis.massa@nunc.edu',
        'birthDate': '31/12/1969',
        'basicSalary': '3.000.000',
        'status': 'Divorced',
        'group': 'Research and Development',
        'description': '16/05/2020'
      },
      {
        'username': 'Armando',
        'firstName': 'Troy',
        'lastName': 'Richard',
        'email': 'pellentesque@convallisincursus.ca',
        'birthDate': '31/12/1969',
        'basicSalary': '3.000.000',
        'status': 'Divorced',
        'group': 'Sales and Marketing',
        'description': '12/01/2020'
      },
      {
        'username': 'Lamar',
        'firstName': 'Hashim',
        'lastName': 'Victor',
        'email': 'Vestibulum@tortor.net',
        'birthDate': '31/12/1969',
        'basicSalary': '0',
        'status': 'Married',
        'group': 'Customer Service',
        'description': '25/08/2020'
      },
      {
        'username': 'Zachary',
        'firstName': 'Wang',
        'lastName': 'Arthur',
        'email': 'augue@temporestac.edu',
        'birthDate': '31/12/1969',
        'basicSalary': '9.000.000',
        'status': 'Married',
        'group': 'Research and Development',
        'description': '09/02/2019'
      },
      {
        'username': 'Phelan',
        'firstName': 'Rooney',
        'lastName': 'August',
        'email': 'semper.rutrum@molestiearcuSed.com',
        'birthDate': '31/12/1969',
        'basicSalary': '2.000.000',
        'status': 'Married',
        'group': 'Sales and Marketing',
        'description': '06/10/2019'
      },
      {
        'username': 'Lawrence',
        'firstName': 'Dennis',
        'lastName': 'Grant',
        'email': 'ornare.In.faucibus@magnatellusfaucibus.co.uk',
        'birthDate': '31/12/1969',
        'basicSalary': '6.000.000',
        'status': 'Married',
        'group': 'Customer Service',
        'description': '09/06/2019'
      },
      {
        'username': 'Geoffrey',
        'firstName': 'Gabriel',
        'lastName': 'Yardley',
        'email': 'nec.mollis.vitae@velit.net',
        'birthDate': '31/12/1969',
        'basicSalary': '5.000.000',
        'status': 'Divorced',
        'group': 'Research and Development',
        'description': '05/03/2019'
      },
      {
        'username': 'Adam',
        'firstName': 'Kasper',
        'lastName': 'Justin',
        'email': 'ac.ipsum@Morbi.com',
        'birthDate': '31/12/1969',
        'basicSalary': '2.000.000',
        'status': 'Married',
        'group': 'Finances',
        'description': '07/02/2019'
      },
      {
        'username': 'Hakeem',
        'firstName': 'Aladdin',
        'lastName': 'Rashad',
        'email': 'per.conubia@metusurnaconvallis.org',
        'birthDate': '31/12/1969',
        'basicSalary': '3.000.000',
        'status': 'Married',
        'group': 'Payroll',
        'description': '02/04/2019'
      },
      {
        'username': 'Alfonso',
        'firstName': 'Nash',
        'lastName': 'Gray',
        'email': 'Cras.sed.leo@laciniavitaesodales.edu',
        'birthDate': '31/12/1969',
        'basicSalary': '3.000.000',
        'status': 'Common-Law',
        'group': 'Asset Management',
        'description': '02/12/2019'
      },
      {
        'username': 'Evan',
        'firstName': 'Chester',
        'lastName': 'Reese',
        'email': 'amet.ante@loremac.org',
        'birthDate': '31/12/1969',
        'basicSalary': '0',
        'status': 'Married',
        'group': 'Quality Assurance',
        'description': '12/03/2020'
      },
      {
        'username': 'Isaac',
        'firstName': 'Isaac',
        'lastName': 'Victor',
        'email': 'in.consectetuer.ipsum@enimEtiamgravida.edu',
        'birthDate': '31/12/1969',
        'basicSalary': '9.000.000',
        'status': 'Common-Law',
        'group': 'Media Relations',
        'description': '03/04/2020'
      },
      {
        'username': 'Uriah',
        'firstName': 'Dorian',
        'lastName': 'Elliott',
        'email': 'mauris@etlacinia.co.uk',
        'birthDate': '31/12/1969',
        'basicSalary': '9.000.000',
        'status': 'Single',
        'group': 'Advertising',
        'description': '04/11/2019'
      },
      {
        'username': 'Duncan',
        'firstName': 'Rashad',
        'lastName': 'Howard',
        'email': 'eu.accumsan.sed@egetipsumDonec.net',
        'birthDate': '31/12/1969',
        'basicSalary': '0',
        'status': 'Divorced',
        'group': 'Public Relations',
        'description': '07/03/2020'
      },
      {
        'username': 'Leonard',
        'firstName': 'Peter',
        'lastName': 'Gannon',
        'email': 'nascetur.ridiculus@et.com',
        'birthDate': '31/12/1969',
        'basicSalary': '7.000.000',
        'status': 'Single',
        'group': 'Accounting',
        'description': '10/09/2020'
      },
      {
        'username': 'Barclay',
        'firstName': 'Ulysses',
        'lastName': 'Stuart',
        'email': 'et.magnis@interdum.net',
        'birthDate': '31/12/1969',
        'basicSalary': '5.000.000',
        'status': 'Married',
        'group': 'Accounting',
        'description': '21/11/2019'
      },
      {
        'username': 'Nigel',
        'firstName': 'Daniel',
        'lastName': 'Jamal',
        'email': 'volutpat@idanteNunc.net',
        'birthDate': '31/12/1969',
        'basicSalary': '6.000.000',
        'status': 'Married',
        'group': 'Advertising',
        'description': '31/05/2019'
      },
      {
        'username': 'Tiger',
        'firstName': 'Jarrod',
        'lastName': 'Daniel',
        'email': 'Nullam.ut.nisi@neque.org',
        'birthDate': '31/12/1969',
        'basicSalary': '8.000.000',
        'status': 'Common-Law',
        'group': 'Accounting',
        'description': '20/05/2020'
      },
      {
        'username': 'Xenos',
        'firstName': 'Evan',
        'lastName': 'Kennedy',
        'email': 'Nam.tempor.diam@imperdietdictummagna.net',
        'birthDate': '31/12/1969',
        'basicSalary': '0',
        'status': 'Divorced',
        'group': 'Sales and Marketing',
        'description': '04/02/2019'
      },
      {
        'username': 'Warren',
        'firstName': 'Jerry',
        'lastName': 'Hamilton',
        'email': 'dui.Fusce@tinciduntpede.net',
        'birthDate': '31/12/1969',
        'basicSalary': '3.000.000',
        'status': 'Married',
        'group': 'Legal Department',
        'description': '28/11/2018'
      },
      {
        'username': 'Amir',
        'firstName': 'Tanner',
        'lastName': 'Abel',
        'email': 'ipsum@ultriciesligulaNullam.net',
        'birthDate': '31/12/1969',
        'basicSalary': '8.000.000',
        'status': 'Married',
        'group': 'Finances',
        'description': '07/08/2020'
      },
      {
        'username': 'Stuart',
        'firstName': 'Levi',
        'lastName': 'Oscar',
        'email': 'auctor@fringillapurusmauris.ca',
        'birthDate': '31/12/1969',
        'basicSalary': '9.000.000',
        'status': 'Divorced',
        'group': 'Customer Relations',
        'description': '03/07/2019'
      },
      {
        'username': 'Dylan',
        'firstName': 'Driscoll',
        'lastName': 'Norman',
        'email': 'nascetur@acmattis.com',
        'birthDate': '31/12/1969',
        'basicSalary': '3.000.000',
        'status': 'Common-Law',
        'group': 'Customer Service',
        'description': '17/12/2019'
      },
      {
        'username': 'Keaton',
        'firstName': 'Mannix',
        'lastName': 'Peter',
        'email': 'dapibus@inconsectetuer.org',
        'birthDate': '31/12/1969',
        'basicSalary': '6.000.000',
        'status': 'Married',
        'group': 'Legal Department',
        'description': '18/07/2020'
      },
      {
        'username': 'Tanek',
        'firstName': 'Nehru',
        'lastName': 'Reed',
        'email': 'nunc@ac.org',
        'birthDate': '31/12/1969',
        'basicSalary': '9.000.000',
        'status': 'Married',
        'group': 'Legal Department',
        'description': '10/07/2020'
      },
      {
        'username': 'Upton',
        'firstName': 'Austin',
        'lastName': 'Lane',
        'email': 'purus.Nullam@faucibus.net',
        'birthDate': '31/12/1969',
        'basicSalary': '0',
        'status': 'Divorced',
        'group': 'Quality Assurance',
        'description': '19/05/2019'
      },
      {
        'username': 'Dorian',
        'firstName': 'Cole',
        'lastName': 'Curran',
        'email': 'ut.nisi@dapibusidblandit.edu',
        'birthDate': '31/12/1969',
        'basicSalary': '2.000.000',
        'status': 'Single',
        'group': 'Quality Assurance',
        'description': '19/09/2019'
      },
      {
        'username': 'Leonard',
        'firstName': 'Vaughan',
        'lastName': 'Cain',
        'email': 'magnis.dis.parturient@mollisPhasellus.com',
        'birthDate': '31/12/1969',
        'basicSalary': '7.000.000',
        'status': 'Common-Law',
        'group': 'Research and Development',
        'description': '14/09/2020'
      },
      {
        'username': 'Neville',
        'firstName': 'Leroy',
        'lastName': 'Caleb',
        'email': 'eu@vitae.org',
        'birthDate': '31/12/1969',
        'basicSalary': '5.000.000',
        'status': 'Married',
        'group': 'Payroll',
        'description': '23/03/2019'
      },
      {
        'username': 'Peter',
        'firstName': 'Jermaine',
        'lastName': 'Phillip',
        'email': 'vehicula.et@posuereenim.org',
        'birthDate': '31/12/1969',
        'basicSalary': '2.000.000',
        'status': 'Common-Law',
        'group': 'Customer Service',
        'description': '10/02/2019'
      },
      {
        'username': 'Dustin',
        'firstName': 'Walker',
        'lastName': 'Kato',
        'email': 'placerat.orci.lacus@sitamet.org',
        'birthDate': '31/12/1969',
        'basicSalary': '7.000.000',
        'status': 'Married',
        'group': 'Accounting',
        'description': '11/10/2019'
      },
      {
        'username': 'Brady',
        'firstName': 'Lars',
        'lastName': 'Arsenio',
        'email': 'erat.vitae@iaculisodioNam.org',
        'birthDate': '31/12/1969',
        'basicSalary': '8.000.000',
        'status': 'Divorced',
        'group': 'Accounting',
        'description': '11/09/2019'
      },
      {
        'username': 'Dante',
        'firstName': 'Blaze',
        'lastName': 'Laith',
        'email': 'lorem.lorem@lacusQuisqueimperdiet.ca',
        'birthDate': '31/12/1969',
        'basicSalary': '9.000.000',
        'status': 'Divorced',
        'group': 'Sales and Marketing',
        'description': '16/09/2020'
      },
      {
        'username': 'Oren',
        'firstName': 'Baxter',
        'lastName': 'Brock',
        'email': 'in.consectetuer.ipsum@ut.org',
        'birthDate': '31/12/1969',
        'basicSalary': '9.000.000',
        'status': 'Married',
        'group': 'Finances',
        'description': '21/10/2018'
      },
      {
        'username': 'Xanthus',
        'firstName': 'Joshua',
        'lastName': 'Cullen',
        'email': 'aliquam.enim@mi.ca',
        'birthDate': '31/12/1969',
        'basicSalary': '2.000.000',
        'status': 'Divorced',
        'group': 'Public Relations',
        'description': '01/04/2020'
      },
      {
        'username': 'Macon',
        'firstName': 'Henry',
        'lastName': 'Martin',
        'email': 'Proin@dapibusidblandit.co.uk',
        'birthDate': '31/12/1969',
        'basicSalary': '4.000.000',
        'status': 'Divorced',
        'group': 'Sales and Marketing',
        'description': '14/03/2019'
      },
      {
        'username': 'Kirk',
        'firstName': 'Ian',
        'lastName': 'Wang',
        'email': 'sagittis@sed.ca',
        'birthDate': '31/12/1969',
        'basicSalary': '2.000.000',
        'status': 'Common-Law',
        'group': 'Public Relations',
        'description': '23/06/2019'
      },
      {
        'username': 'Malcolm',
        'firstName': 'Griffin',
        'lastName': 'Herrod',
        'email': 'nisi.a@vitae.org',
        'birthDate': '31/12/1969',
        'basicSalary': '8.000.000',
        'status': 'Common-Law',
        'group': 'Asset Management',
        'description': '01/12/2018'
      },
      {
        'username': 'Jameson',
        'firstName': 'Knox',
        'lastName': 'Simon',
        'email': 'Nullam.feugiat.placerat@ametorciUt.org',
        'birthDate': '31/12/1969',
        'basicSalary': '0',
        'status': 'Single',
        'group': 'Public Relations',
        'description': '19/05/2020'
      },
      {
        'username': 'Chester',
        'firstName': 'Tyler',
        'lastName': 'Carlos',
        'email': 'sed@sit.edu',
        'birthDate': '31/12/1969',
        'basicSalary': '6.000.000',
        'status': 'Married',
        'group': 'Media Relations',
        'description': '30/12/2018'
      },
      {
        'username': 'Ulysses',
        'firstName': 'Jin',
        'lastName': 'Colorado',
        'email': 'consequat.lectus.sit@Namtempordiam.edu',
        'birthDate': '31/12/1969',
        'basicSalary': '6.000.000',
        'status': 'Married',
        'group': 'Media Relations',
        'description': '27/08/2020'
      },
      {
        'username': 'Rigel',
        'firstName': 'Micah',
        'lastName': 'Kyle',
        'email': 'Donec.luctus.aliquet@dapibus.com',
        'birthDate': '31/12/1969',
        'basicSalary': '0',
        'status': 'Divorced',
        'group': 'Quality Assurance',
        'description': '23/04/2020'
      },
      {
        'username': 'Moses',
        'firstName': 'Adam',
        'lastName': 'Deacon',
        'email': 'pharetra@euaccumsansed.edu',
        'birthDate': '31/12/1969',
        'basicSalary': '2.000.000',
        'status': 'Single',
        'group': 'Asset Management',
        'description': '18/05/2019'
      },
      {
        'username': 'Cullen',
        'firstName': 'Jesse',
        'lastName': 'Uriah',
        'email': 'nec.cursus@feugiatnec.net',
        'birthDate': '31/12/1969',
        'basicSalary': '7.000.000',
        'status': 'Divorced',
        'group': 'Quality Assurance',
        'description': '03/03/2020'
      },
      {
        'username': 'Gavin',
        'firstName': 'Merrill',
        'lastName': 'Axel',
        'email': 'sociis.natoque@egetipsum.edu',
        'birthDate': '31/12/1969',
        'basicSalary': '8.000.000',
        'status': 'Single',
        'group': 'Quality Assurance',
        'description': '12/01/2019'
      },
      {
        'username': 'Tiger',
        'firstName': 'Garth',
        'lastName': 'Driscoll',
        'email': 'Donec.egestas@sapien.edu',
        'birthDate': '31/12/1969',
        'basicSalary': '8.000.000',
        'status': 'Divorced',
        'group': 'Public Relations',
        'description': '13/11/2018'
      },
      {
        'username': 'Vance',
        'firstName': 'Kennan',
        'lastName': 'Chadwick',
        'email': 'vulputate@Aenean.net',
        'birthDate': '31/12/1969',
        'basicSalary': '8.000.000',
        'status': 'Single',
        'group': 'Research and Development',
        'description': '12/09/2019'
      },
      {
        'username': 'Bruno',
        'firstName': 'Hiram',
        'lastName': 'Carl',
        'email': 'est.Nunc.ullamcorper@Mauris.org',
        'birthDate': '31/12/1969',
        'basicSalary': '3.000.000',
        'status': 'Single',
        'group': 'Finances',
        'description': '13/08/2020'
      },
      {
        'username': 'Price',
        'firstName': 'Orson',
        'lastName': 'Zephania',
        'email': 'neque@Aenean.org',
        'birthDate': '31/12/1969',
        'basicSalary': '8.000.000',
        'status': 'Married',
        'group': 'Accounting',
        'description': '11/10/2018'
      },
      {
        'username': 'James',
        'firstName': 'Owen',
        'lastName': 'Lester',
        'email': 'netus.et.malesuada@ornarelectusante.net',
        'birthDate': '31/12/1969',
        'basicSalary': '9.000.000',
        'status': 'Married',
        'group': 'Asset Management',
        'description': '31/03/2019'
      },
      {
        'username': 'Aaron',
        'firstName': 'Lee',
        'lastName': 'Lucius',
        'email': 'Nam.porttitor.scelerisque@dictum.edu',
        'birthDate': '31/12/1969',
        'basicSalary': '7.000.000',
        'status': 'Common-Law',
        'group': 'Sales and Marketing',
        'description': '05/05/2019'
      },
      {
        'username': 'Zachary',
        'firstName': 'Kevin',
        'lastName': 'Gage',
        'email': 'montes.nascetur@velvenenatisvel.co.uk',
        'birthDate': '31/12/1969',
        'basicSalary': '9.000.000',
        'status': 'Common-Law',
        'group': 'Public Relations',
        'description': '06/09/2019'
      },
      {
        'username': 'Arsenio',
        'firstName': 'Quentin',
        'lastName': 'Aristotle',
        'email': 'nec@Mauris.org',
        'birthDate': '31/12/1969',
        'basicSalary': '9.000.000',
        'status': 'Single',
        'group': 'Asset Management',
        'description': '26/11/2019'
      },
      {
        'username': 'Raja',
        'firstName': 'Dustin',
        'lastName': 'Hakeem',
        'email': 'viverra.Maecenas.iaculis@inhendreritconsectetuer.co.uk',
        'birthDate': '31/12/1969',
        'basicSalary': '5.000.000',
        'status': 'Single',
        'group': 'Asset Management',
        'description': '30/11/2019'
      },
      {
        'username': 'Cain',
        'firstName': 'Adrian',
        'lastName': 'Zane',
        'email': 'Phasellus.dapibus@afelisullamcorper.net',
        'birthDate': '31/12/1969',
        'basicSalary': '5.000.000',
        'status': 'Married',
        'group': 'Quality Assurance',
        'description': '22/08/2020'
      },
      {
        'username': 'Wade',
        'firstName': 'Allen',
        'lastName': 'Judah',
        'email': 'in@dictum.edu',
        'birthDate': '31/12/1969',
        'basicSalary': '7.000.000',
        'status': 'Married',
        'group': 'Payroll',
        'description': '14/05/2019'
      },
      {
        'username': 'Tiger',
        'firstName': 'Buckminster',
        'lastName': 'Reuben',
        'email': 'quis.pede.Praesent@Maurisnon.org',
        'birthDate': '31/12/1969',
        'basicSalary': '8.000.000',
        'status': 'Divorced',
        'group': 'Human Resources',
        'description': '07/11/2019'
      },
      {
        'username': 'Dustin',
        'firstName': 'Gray',
        'lastName': 'Abel',
        'email': 'laoreet.posuere.enim@egetvolutpat.net',
        'birthDate': '31/12/1969',
        'basicSalary': '4.000.000',
        'status': 'Common-Law',
        'group': 'Payroll',
        'description': '07/11/2019'
      },
      {
        'username': 'Noble',
        'firstName': 'Reed',
        'lastName': 'Kadeem',
        'email': 'felis.adipiscing.fringilla@vitaerisus.net',
        'birthDate': '31/12/1969',
        'basicSalary': '8.000.000',
        'status': 'Divorced',
        'group': 'Media Relations',
        'description': '03/05/2019'
      },
      {
        'username': 'Octavius',
        'firstName': 'Shad',
        'lastName': 'Hall',
        'email': 'Nullam.feugiat@Suspendissealiquet.org',
        'birthDate': '31/12/1969',
        'basicSalary': '9.000.000',
        'status': 'Married',
        'group': 'Finances',
        'description': '26/02/2020'
      },
      {
        'username': 'Beck',
        'firstName': 'Dustin',
        'lastName': 'Xander',
        'email': 'Duis.ac@consectetueradipiscing.net',
        'birthDate': '31/12/1969',
        'basicSalary': '3.000.000',
        'status': 'Married',
        'group': 'Quality Assurance',
        'description': '13/01/2019'
      },
      {
        'username': 'Aladdin',
        'firstName': 'Zachery',
        'lastName': 'Chester',
        'email': 'egestas.a.dui@Curabitur.ca',
        'birthDate': '31/12/1969',
        'basicSalary': '7.000.000',
        'status': 'Divorced',
        'group': 'Tech Support',
        'description': '17/02/2019'
      },
      {
        'username': 'Thaddeus',
        'firstName': 'Hammett',
        'lastName': 'Bruno',
        'email': 'eleifend.Cras.sed@loremipsum.com',
        'birthDate': '31/12/1969',
        'basicSalary': '8.000.000',
        'status': 'Single',
        'group': 'Public Relations',
        'description': '21/03/2019'
      },
      {
        'username': 'Wallace',
        'firstName': 'Giacomo',
        'lastName': 'Tad',
        'email': 'risus@Etiamlaoreet.edu',
        'birthDate': '31/12/1969',
        'basicSalary': '9.000.000',
        'status': 'Single',
        'group': 'Legal Department',
        'description': '14/04/2019'
      },
      {
        'username': 'Ciaran',
        'firstName': 'Blaze',
        'lastName': 'Clinton',
        'email': 'augue.ut@habitant.ca',
        'birthDate': '31/12/1969',
        'basicSalary': '3.000.000',
        'status': 'Single',
        'group': 'Legal Department',
        'description': '10/06/2019'
      }
    ];
    this.groupData = [
      {id: 1, name: 'Accounting'},
      {id: 2, name: 'Kaunas'},
      {id: 3, name: 'Advertising'},
      {id: 4, name: 'Asset Management'},
      {id: 5, name: 'Customer Relations'},
      {id: 6, name: 'Customer Service'},
      {id: 7, name: 'Finances'},
      {id: 8, name: 'Human Resources'},
      {id: 9, name: 'Legal Department'},
      {id: 10, name: 'Media Relations'},
      {id: 11, name: 'Payroll'},
      {id: 12, name: 'Public Relations'},
      {id: 13, name: 'Quality Assurance'},
      {id: 14, name: 'Research and Development'},
      {id: 15, name: 'Tech Support'},
    ];
    this.minDate = new Date();
    this.maxDate = new Date(formatDate(this.minDate, 'yyyy/MM/dd', 'en'));
    console.log(formatDate(this.minDate, 'yyyy/MM/dd', 'en'));
    // this.selectGroup = [
    //   {'group': 'Accounting'},
    //   {'group': 'Advertising'},
    //   {'group': 'Asset Management'},
    //   {'group': 'Customer Relations'},
    //   {'group': 'Customer Service'},
    //   {'group': 'Finances'},
    //   {'group': 'Human Resources'},
    //   {'group': 'Legal Department'},
    //   {'group': 'Media Relations'},
    //   {'group': 'Payroll'},
    //   {'group': 'Public Relations'},
    //   {'group': 'Quality Assurance'},
    //   {'group': 'Sales and Marketing'},
    //   {'group': 'Research and Development'},
    //   {'group': 'Tech Support'},
    // ];
  }
  formAdd() {
    document.getElementById('formAdd').style.display = 'inline';
    document.getElementById('listData').style.display = 'none';
    document.getElementById('formUpdate').style.display = 'none';
    document.getElementById('DetailData').style.display = 'none';
  }
  onSubmit(data) {
    console.log(data);
    this.formGroup.reset();
  }
  formUpdate(user) {
    this.Updusername.nativeElement.value = user.username;
    this.UpdfirstName.nativeElement.value = user.firstName;
    this.UpdlastName.nativeElement.value = user.lastName;
    this.Updemail.nativeElement.value = user.email;
    this.UpdbirthDate.nativeElement.value = user.birthDate;
    this.UpdbasicSalary.nativeElement.value = user.basicSalary;
    this.Updstatus.nativeElement.value = user.status;
    this.Updgroup.nativeElement.value = user.group;
    this.Upddescription.nativeElement.value = user.description;
    document.getElementById('formAdd').style.display = 'none';
    document.getElementById('listData').style.display = 'none';
    document.getElementById('formUpdate').style.display = 'inline';
    document.getElementById('DetailData').style.display = 'none';
  }
  DetailData(user) {
    // alert('add');
    console.log(user);
    this.Viewusername.nativeElement.value = user.username;
    this.ViewfirstName.nativeElement.value = user.firstName;
    this.ViewlastName.nativeElement.value = user.lastName;
    this.Viewemail.nativeElement.value = user.email;
    this.ViewbirthDate.nativeElement.value = user.birthDate;
    this.ViewbasicSalary.nativeElement.value = user.basicSalary;
    this.Viewstatus.nativeElement.value = user.status;
    this.Viewgroup.nativeElement.value = user.group;
    this.Viewdescription.nativeElement.value = user.description;
    document.getElementById('DetailData').style.display = 'inline';
    document.getElementById('formAdd').style.display = 'none';
    document.getElementById('formUpdate').style.display = 'none';
    document.getElementById('listData').style.display = 'none';
  }
  cancelForm() {
    this.formGroup.reset();
    document.getElementById('DetailData').style.display = 'none';
    document.getElementById('formAdd').style.display = 'none';
    document.getElementById('formUpdate').style.display = 'none';
    document.getElementById('listData').style.display = 'inline';
  }
  DeleteData() {
    document.getElementById('NotifDelete').style.display = 'inline';
  }
  closeNotif() {
    document.getElementById('NotifDelete').style.display = 'none';
  }
  logout() {
    this.router.navigate(['']);
  }
}
