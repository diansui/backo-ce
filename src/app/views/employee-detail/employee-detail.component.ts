import { Component, OnInit } from '@angular/core';
import {FormControl, Validators} from '@angular/forms';
import {MyErrorStateMatcher} from '../employee-form-add/employee-form-add.component';
import {formatDate} from '@angular/common';
import {Router} from '@angular/router';

@Component({
  selector: 'app-employee-detail',
  templateUrl: './employee-detail.component.html',
  styleUrls: ['./employee-detail.component.css']
})
export class EmployeeDetailComponent implements OnInit {
  item;
  minDate;
  maxDate;
  value;
  DateDOB;
  groupData = [
    {id: 1, name: 'Accounting'},
    {id: 2, name: 'Kaunas'},
    {id: 3, name: 'Advertising'},
    {id: 4, name: 'Asset Management'},
    {id: 5, name: 'Customer Relations'},
    {id: 6, name: 'Customer Service'},
    {id: 7, name: 'Finances'},
    {id: 8, name: 'Human Resources'},
    {id: 9, name: 'Legal Department'},
    {id: 10, name: 'Media Relations'},
    {id: 11, name: 'Payroll'},
    {id: 12, name: 'Public Relations'},
    {id: 13, name: 'Quality Assurance'},
    {id: 14, name: 'Research and Development'},
    {id: 15, name: 'Tech Support'},
  ];
  emailFormControl = new FormControl('', [
    Validators.required, Validators.email,
  ]);
  groupControl = new FormControl('', [Validators.required]);
  descriptionControl = new FormControl('', [Validators.required]);
  UserNameControl = new FormControl('', [Validators.required]);
  FirstNameControl = new FormControl('', [Validators.required]);
  LastNameControl = new FormControl('', [Validators.required]);
  StatusControl = new FormControl('', [Validators.required]);
  DOBFormControl = new FormControl('', [Validators.required]);
  BasicSalaryFormControl = new FormControl('', [Validators.required]);
  matcher = new MyErrorStateMatcher();
  constructor(private router: Router) { }

  ngOnInit() {
    this.item = JSON.parse(localStorage.getItem('detailEmployee'));
    console.log(JSON.parse(localStorage.getItem('detailEmployee')));
    this.minDate = new Date();
    this.maxDate = new Date(formatDate(this.minDate, 'yyyy/MM/dd', 'en'));
    this.DateDOB = formatDate(this.minDate, 'yyyy/MM/dd', 'en');
    console.log(formatDate(this.minDate, 'yyyy/MM/dd', 'en'));
  }

  btnCancel() {
    this.router.navigate(['employee']);
  }

}
